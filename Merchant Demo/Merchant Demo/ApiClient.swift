//
//  Copyright © 2019 Getswish AB and its affiliates.
//
//  This source code is licensed under the MIT license found in the
//  LICENSE file in the root directory of this source tree.
//

import Foundation

class ApiClient : NSObject {
    
    public enum StringConstants: String {
        case baseUrl = "http://YOUR-IP-HERE:3000/"
    }
    
    func createMCommercePaymentRequest(amount: String, message: String?, completion: @escaping (Bool, [String : String]) -> Void) {
        let request = URLRequest(url: URL(string: StringConstants.baseUrl.rawValue + "paymentrequests")!)
        let json = ["amount" : amount, "message" : message ?? ""]
        post(request: request, body: json, completion: completion)
    }
    
    func getPaymentRequest(id: String, completion: @escaping (Bool, [String : String]) -> Void) {
        let request = URLRequest(url: URL(string: StringConstants.baseUrl.rawValue + "paymentrequests" + "/" + id)!)
        get(request: request, completion: completion)
    }
    
    func createRefund(originalPaymentReference:String, amount: String, message: String?, completion: @escaping (Bool, [String : String]) -> Void) {
        let request = URLRequest(url: URL(string: StringConstants.baseUrl.rawValue + "refunds")!)
        let json = ["originalPaymentReference" : originalPaymentReference, "amount" : amount, "message" : message ?? ""]
        post(request: request, body: json, completion: completion)
    }
    
    func getRefund(id: String, completion: @escaping (Bool, [String : String]) -> Void) {
        let request = URLRequest(url: URL(string: StringConstants.baseUrl.rawValue + "refunds" + "/" + id)!)
        get(request: request, completion: completion)
    }
}

private extension ApiClient {
    func post(request: URLRequest, body: Any, completion: @escaping (Bool, [String : String]) -> Void) {
        var request = request
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            try request.httpBody = JSONSerialization.data(withJSONObject: body, options: [])
        } catch {
            fatalError("Error occurred while encoding JSON")
        }
        jsonTask(request: request, method: "POST", completion: completion)
    }
    
    func put(request: URLRequest, completion: @escaping (Bool, [String : String]) -> Void) {
        jsonTask(request: request, method: "PUT", completion: completion)
    }
    
    func get(request: URLRequest, completion: @escaping (Bool, [String : String]) -> Void) {
        jsonTask(request: request, method: "GET", completion: completion)
    }
    
    func get(request: URLRequest, completion: @escaping (Bool, Data) -> Void) {
        dataTask(request: request, method: "GET", completion: completion)
    }
    
    func jsonTask(request: URLRequest, method: String, completion: @escaping (Bool, [String : String]) -> Void) {
        var request = request
        request.httpMethod = method
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        session.dataTask(with: request as URLRequest) { (data, response, error) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                guard let jsonDictionary = json as? [String : String] else {
                    completion(false, [:])
                    return
                }
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode {
                    completion(true, jsonDictionary)
                } else {
                    completion(false, [:])
                }
            }
        }.resume()
    }
    
    func dataTask(request: URLRequest, method: String, completion: @escaping (Bool, Data) -> Void) {
        var request = request
        request.httpMethod = method
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        session.dataTask(with: request as URLRequest) { (data, response, error) in
            if let data = data {
                completion(true, data)
            } else {
                completion(false, Data())
            }
        }.resume()
    }
}
