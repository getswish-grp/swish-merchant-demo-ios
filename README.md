# Swish Merchant Demo iOS
## About
This project demonstrates how to integrate Swish in an iOS app as a merchant. The demo app targets a local Swish Merchant Demo Web installation by default. You can run the Swish Merchant Demo Web on a local machine to test the API calls.

## Examples
### Checking if the Swish app is installed
```swift
enum StringConstants: String {
    case Host = "paymentrequest"
    case SwishUrl = "swish://"
    case MerchantCallbackUrl = "merchantdemo://"
    case Scheme = "swish"
}
    
func isSwishAppInstalled() -> Bool {
    guard let url = URL(string: StringConstants.SwishUrl.rawValue) else {
        preconditionFailure("Invalid url")
    }
    return UIApplication.shared.canOpenURL(url)
}
```
### Switching to the Swish app
```swift
enum StringConstants: String {
    case Host = "paymentrequest"
    case SwishUrl = "swish://"
    case MerchantCallbackUrl = "merchantdemo://"
    case Scheme = "swish"
}
    
func openSwishAppWithToken(_ token: String) {
    guard isSwishAppInstalled() else {
        // Swish app is not installed, show error
        return
    }
    
    guard let callback = encodedCallbackUrl() else {
        preconditionFailure("Callback url is required")
    }
    
    var urlComponents = URLComponents()
    urlComponents.host = StringConstants.Host.rawValue
    urlComponents.scheme = StringConstants.Scheme.rawValue
    urlComponents.queryItems = [URLQueryItem(name: "token", value: token),
                                URLQueryItem(name: "callbackurl", value: callback)]
    
    guard let url = urlComponents.url else {
        preconditionFailure("Invalid url")
    }
    
    UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
        if !success {
            // The URL could not be opened, show error
        }
    })
}      

func encodedCallbackUrl() -> String? {
    let callback = StringConstants.MerchantCallbackUrl.rawValue
    let disallowedCharacters = NSCharacterSet(charactersIn: "!*'();:@&=+$,/?%#[]")
    let allowedCharacters = disallowedCharacters.inverted
    return callback.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
}
  
func isSwishAppInstalled() -> Bool {
    guard let url = URL(string: StringConstants.SwishUrl.rawValue) else {
        preconditionFailure("Invalid url")
    }
    return UIApplication.shared.canOpenURL(url)
}
```
### M-Commerce Payment Flow
The demo app demonstrates an M-Commerce payment flow typically used in mobile applications. This flow can be tested using the Swish Merchant Demo Web running on a local machine. Make sure that the web server is running and change the following line to your IP address in `ApiClient.swift`:

```swift
public enum StringConstants: String {
	case baseUrl = "http://YOUR-IP-HERE:3000/"
}
```