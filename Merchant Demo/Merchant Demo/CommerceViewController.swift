//
//  Copyright © 2019 Getswish AB and its affiliates.
//
//  This source code is licensed under the MIT license found in the
//  LICENSE file in the root directory of this source tree.
//

import UIKit

class CommerceViewController: UIViewController {
    
    private var commerceType:CommerceType = .mcom
    
    public enum CommerceType: String {
        case mcom = "M-Commerce"
    }
    
    @IBOutlet weak private var scrollView: UIScrollView!

    @IBOutlet weak private var amountField: UITextField!
    @IBOutlet weak private var messageField: UITextField!
    @IBOutlet weak private var statusLabel: UILabel!
    @IBOutlet weak private var startButton: UIButton!
    
    @IBOutlet weak private var refundStackView: UIStackView!
    @IBOutlet weak private var refundButton: UIButton!
    
    let apiClient = ApiClient()
    let swishClient = SwishClient()
    
    var id:String? = nil
    var refundId:String? = nil
    var paymentReference:String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateStatus("Ready to create new payment request")
        
        amountField.delegate = self
        messageField.delegate = self
        
        self.scrollView.keyboardDismissMode = .onDrag
                
        title = commerceType.rawValue
        
        clear()
    }
    
    func segueInit(_ commerceType:CommerceType) {
        self.commerceType = commerceType
    }
    
    func updateStatus(_ status: String) {
        statusLabel.text = "Status: \(status)"
    }
    
    // MARK: Actions
    
    @IBAction func didPressUpdateRefundStatusButton(_ sender: Any) {
        guard let id = self.refundId else {
            handleRefundError("No refund id found")
            return
        }
        
        apiClient.getRefund(id: id) {[weak self] (success, result) in
            DispatchQueue.main.async {
                
                guard let weakSelf = self else {
                    return
                }
                
                guard let refundId = result["id"], let status = result["status"], let originalPaymentReference = result["originalPaymentReference"], success else {
                    weakSelf.handleError("Refund Confirmation Error")
                    return
                }
                weakSelf.paymentReference = originalPaymentReference
                weakSelf.refundId = refundId
                
                if status == "PAID" {
                    weakSelf.updateStatus("\(status) for refund for payment with reference: \(originalPaymentReference)")
                } else {
                    weakSelf.updateStatus("\(status) for refund for payment with refund-id: \(id)")
                }
            }
        }
    }
    
    @IBAction func didPressRefundButton(_ sender: Any) {
        
        guard let paymentReference = self.paymentReference else {
            self.handleError("No Payment Reference found")
            return
        }
        
        guard let amount = amountField.text, amount.count > 0 else {
            self.handleError("Amount is required")
            return
        }
        
        let message = messageField.text ?? ""
        
        apiClient.createRefund(originalPaymentReference: paymentReference, amount: amount, message: message) {[weak self] (success, result) in
            DispatchQueue.main.async {
                
                guard let weakSelf = self else {
                    return
                }
                
                guard let refundId = result["id"], let status = result["status"], let originalPaymentReference = result["originalPaymentReference"], success else {
                    weakSelf.handleRefundError("Failed to create new refund request")
                    return
                }
                weakSelf.paymentReference = paymentReference
                weakSelf.refundId = refundId
                weakSelf.updateStatus("\(status) for refund with paymentreference: \(originalPaymentReference)")
            }
        }
    }
    
    @IBAction func didPressCheckStatusButton(_ sender: Any) {
        
        guard let id = self.id else {
            handleError("No id found")
            return
        }
        
        apiClient.getPaymentRequest(id: id) {[weak self] (success, result) in
            DispatchQueue.main.async {
                guard let weakSelf = self else {
                    return
                }
                
                guard let id = result["id"], let status = result["status"], success else {
                    weakSelf.handleError("Request Confirmation Error")
                    return
                }
                weakSelf.id = id
                
                if status == "PAID" {
                    guard let paymentReference = result["paymentReference"] else {
                        weakSelf.handleError("Request Confirmation Error")
                        return
                    }
                    weakSelf.paymentReference = paymentReference
                    weakSelf.updateStatus("\(status) for payment with reference: \(paymentReference)")
                } else {
                    weakSelf.updateStatus("\(status) for payment with id: \(id)")
                }
            }
        }
    }
    
    @IBAction func didPressStartButton(_ sender: Any) {
        clear()
        
        let message = messageField.text ?? ""
        
        guard let amount = amountField.text, amount.count > 0 else {
            self.handleError("Amount is required")
            return
        }
        
        let paymentRequestCompletion: (Bool, [String : String])->Void = {[weak self] (success, result) in
            DispatchQueue.main.async {
                
                guard let weakSelf = self else {
                    return
                }
                
                guard let id = result["id"], success else {
                    weakSelf.handleError("Error, no id found")
                    return
                }
                weakSelf.id = id
                
                guard let token = result["token"] else {
                    weakSelf.handleError("Token not found")
                    return
                }
                
                weakSelf.swishClient.openSwishAppWithToken(token)
                weakSelf.updateStatus("Created request with id: \(id), token: \(token)")
            }
        }
        
        apiClient.createMCommercePaymentRequest(amount: amount, message: message, completion: paymentRequestCompletion)
        
    }

    func handleRefundError(_ error: String) {
        self.updateStatus(error)
    }
    
    func handleError(_ error: String) {
        self.updateStatus(error)
    }
    
    func clear() {
        updateStatus("Ready to create new payment request")
        self.id = nil
        self.refundId = nil
        self.paymentReference = nil
    }
}

extension CommerceViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
