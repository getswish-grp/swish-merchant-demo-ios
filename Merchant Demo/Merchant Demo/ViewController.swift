//
//  Copyright © 2019 Getswish AB and its affiliates.
//
//  This source code is licensed under the MIT license found in the
//  LICENSE file in the root directory of this source tree.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func didPressMComButton(_ sender: Any) {
        performSegue(withIdentifier: "mcom", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let dest = segue.destination as? CommerceViewController else {
            return
        }
        if (segue.identifier == "mcom") {
            dest.segueInit(.mcom)
        }
    }
}

