//
//  Copyright © 2019 Getswish AB and its affiliates.
//
//  This source code is licensed under the MIT license found in the
//  LICENSE file in the root directory of this source tree.
//

import UIKit

class SwishClient {
    
    enum StringConstants: String {
        case Host = "paymentrequest"
        case SwishUrl = "swish://"
        case MerchantCallbackUrl = "merchantdemo://"
        case Scheme = "swish"
    }
    
    func isSwishAppInstalled() -> Bool {
        guard let url = URL(string: StringConstants.SwishUrl.rawValue) else {
            preconditionFailure("Invalid url")
        }
        return UIApplication.shared.canOpenURL(url)
    }
    
    func encodedCallbackUrl() -> String? {
        let callback = StringConstants.MerchantCallbackUrl.rawValue
        let disallowedCharacters = NSCharacterSet(charactersIn: "!*'();:@&=+$,/?%#[]")
        let allowedCharacters = disallowedCharacters.inverted
        return callback.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
    }
    
    func openSwishAppWithToken(_ token: String) {
        guard isSwishAppInstalled() else {
            // Swish app is not installed, show error
            return
        }
        
        guard let callback = encodedCallbackUrl() else {
            preconditionFailure("Callback url is required")
        }
        
        var urlComponents = URLComponents()
        urlComponents.host = StringConstants.Host.rawValue
        urlComponents.scheme = StringConstants.Scheme.rawValue
        urlComponents.queryItems = [URLQueryItem(name: "token", value: token),
                                    URLQueryItem(name: "callbackurl", value: callback)]
        
        guard let url = urlComponents.url else {
            preconditionFailure("Invalid url")
        }
        
        UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
            if !success {
                // The URL could not be opened, show error
            }
        })
    }
    
}
